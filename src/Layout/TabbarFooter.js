import React from 'react'
import { StyleSheet, View, Text, TochableOpacity, Image, ScrollView, ImageBackground } from 'react-native'
import { Icon, IputItem, WhiteSpace, TabBar, Carousel, SearchBar } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push, connectRouter } from 'connected-react-router'
import MapPage from '../Pages/MapPage'
import MainPage from '../Pages/Main'
import ProfilePage from '../Pages/Profile'

class TabBarFooter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'redTab',
        };
    }
    renderContent(pageText) {
        return (
            <View style={{ flex: 1, alignItems: 'center', background: 'white' }}>
                <Text style={{ margin: 50 }}>{pageText}</Text>
            </View>
        );
    }
    onChangeTab(tabName) {
        this.setState({
            selectedTab: tabName,
        });
    }

    render() {

        const { selectedTab = 'blueTab', goToHome, goToMap, goToProfile, goToNavigate } = this.props
        const Tab = selectedTab
        console.log("useasdsadsad", this.props.user.user);
        
        return (
            <View style={{ flex: 1 }}>
                <TabBar
                    unselectedTintColor="#949494"
                    tintColor="#33A3F4"
                    barTintColor="#f5f5f5"
                >
                    <TabBar.Item
                        title="หน้าหลัก"
                        icon={<Icon name="home" />}
                        selected={this.state.selectedTab === 'home'}
                        onPress={() => this.onChangeTab('home')}
                    >
                        <MainPage />
                    </TabBar.Item>

                    <TabBar.Item
                        icon={<Icon name="compass" />}
                        title="แผนที่"
                        selected={this.state.selectedTab === 'map'}
                        onPress={() => this.onChangeTab('map')}
                    >
                        <MapPage />

                    </TabBar.Item>
                    <TabBar.Item
                        icon={<Icon name="heart" />}
                        title="ถูกใจ"
                        selected={this.state.selectedTab === 'heart'}
                        onPress={() => this.onChangeTab('heart')}
                    >

                    </TabBar.Item>
                    <TabBar.Item
                        icon={<Icon name="user" />}
                        title="ข้อมูลส่วนตัว"
                        selected={this.state.selectedTab === 'profile'}
                        onPress={() => this.onChangeTab('profile')}
                    >
                        <ProfilePage />
                    </TabBar.Item>
                </TabBar>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    logo: {
        width: 150,
        height: 150,
        borderRadius: 100,

    },
    wrapper: {
        backgroundColor: '#fff',
        height: 150
    },
    containerHorizontal: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
    },
    containerVertical: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
    },
    text: {
        color: '#fff',
        fontSize: 36,
    }
})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    dispatch => ({
        goToHome: state => dispatch(push('./Home')),
        goToProfile: state => dispatch(push('./Profile')),
        goToMap: state => dispatch(push('./Map'))
    })
)(TabBarFooter)