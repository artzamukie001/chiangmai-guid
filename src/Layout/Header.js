import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, ImageBackground } from 'react-native'
import { Icon, IputItem, WhiteSpace, TabBar, Carousel, SearchBar } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push, connectRouter } from 'connected-react-router'
import Background from '../Image/bg.jpg'
import { goBack } from 'connected-react-router'
import {
    Area
} from '../Component/StyleComponent'

class Header extends React.Component {

    render() {

        const { goBack, title } = this.props

        return (

            <Area style={{ flexDirection: 'row' }}>
                <TouchableOpacity onPress={goBack}>
                    <View style={{ backgroundColor: '#B3B3D9', justifyContent: 'center', flex: 1 }}>
                        <Icon name={'left-circle'} size={40} color={'white'} />
                    </View>
                </TouchableOpacity>

                <View style={{ backgroundColor: '#B3B3D9', flex: 1, alignItems: 'center', height: 60, justifyContent: 'center' }}>
                    <Text style={{ alignItems: 'center', justifyContent: 'center', fontSize: 20, color: 'white' }}>{title}</Text>
                </View>
            </Area>



        )
    }
}


export default connect(null, { goBack })(Header)