import styledComponents from 'styled-components'
import { Button, InputItem, Icon } from '@ant-design/react-native'

export const Containers = styledComponents.View`
flex : 1;
background-color: white;
flex-direction: column;`

export const Area = styledComponents.View`
background-color: white;
`
export const Area2 = styledComponents.View`
flex:1;
background-color: black;
`
export const Imagebackground = styledComponents.ImageBackground`
align-items:center;
width:null;
height:null;
padding-top:35px;
`

export const AreaBox = styledComponents.View`
background-color: rgba(0,0,0,0.1);
align-items:center;
width:300;

`

export const ViewRow = styledComponents.View`
flex-direction:row;
`

export const TextHeader = styledComponents.Text `
font-size: 35px;

color: black;
`

export const TextInput = styledComponents.Text`
font-size: 20px;

color: black;
`

export const ItemInput = styledComponents(InputItem)`
background-color: white;
    color: black; 
    padding-left: 20px;
    height: 100%;
`

export const Logo = styledComponents.ImageBackground`
background-color: white;
width: 150;
height: 150;
border-radius: 300;
`

export const Footers = styledComponents.View`
background-color: rgba(0,0,0,0);
align-items:center;
`
export const TextGeneral = styledComponents.Text`
font-size: 30px;
font-weight: bold;
`

export const ButonLogin = styledComponents(Button)`
background-color: #88DDBB;
`

export const Buttons = styledComponents(Button)`
background-color: #88DDBB;
width:300;
`

export const ButtonFacebook = styledComponents(Button)`
background-color: #A4C8F0;
width:300;
`