import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, ImageBackground } from 'react-native'
import { Icon, IputItem, WhiteSpace, TabBar, Carousel, SearchBar, Grid } from '@ant-design/react-native'
import axios from 'axios'
import Background from '../Image/bg.jpg'
import Headers from '../Layout/Header'
import {
    Containers,
    Imagebackground,
    Area,
    Area2,
    TextHeader,
    Logo
} from '../Component/StyleComponent'

class Shop extends React.Component {
    state = {
        shopList: {},
    }

    componentDidMount() {
        this.ShopList();
    }

    ShopList = () => {
        axios
            .get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0')
            .then(response => {
                console.log("Response: ", response);
                this.setState({
                    shopList: response.data.data,
                    isLoading: false
                })
                console.log('State: ' + this.state.shopList)
                console.log('State.Data: ' + this.state.shopList.data)
            })
            .catch((error) => {
                console.log('ErrorNaja: ' + error);
            })
    }

    goToShopPage = (id) => {
        this.props.history.push('./ShopPage', { id })
    }

    render() {
        
        return (
            <Containers>
                {/* <Area>
               <Text style={{fontSize:40}}>ร้านค้า</Text>
               </Area> */}
                <Area>
                    <Headers
                        title='ร้านค้า'
                    />
                    <ScrollView >
                        <Grid
                            columnNum={1}
                            data={this.state.shopList}
                            itemStyle={{ height: 260, }}
                            renderItem={(item) => (
                                <TouchableOpacity
                                    onPress={() => this.goToShopPage(item.id)}
                                >
                                    <View style={{ height: 45, justifyContent: 'center', paddingLeft: 10 }}>
                                        <Text style={{ color: 'black' ,fontSize:20}}>{item.lang.th.name}</Text>
                                        <Text>{item.category}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: '40%', height: 200, margin: 10 }} source={{ uri: item.image }} />
                                        <View style={{ height: 30, width: '50%' }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flexDirection: 'row', borderWidth: 1, width: 60, height: 30, margin: 5, borderRadius: 10, borderColor: '#CCCCCC' }}>

                                                    <Text style={{ color: 'black', marginLeft: 2, marginTop: 3 }}>4.5</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', borderWidth: 1, width: 70, height: 30, margin: 5, borderRadius: 10, borderColor: '#CCCCCC' }}>

                                                    <Text style={{ color: 'black', marginLeft: 3, marginTop: 3 }}>7 k.m.</Text>
                                                </View>
                                            </View>
                                            <Text style={{ color: 'black', margin: 5 }} numberOfLines={3}>{item.lang.th.description}</Text>
                                        </View>
                                    </View>

                                </TouchableOpacity>

                            )} />
                    </ScrollView>
                </Area>
            </Containers >
        )
    }
}
export default Shop