import React from 'react'
import { View,Alert } from 'react-native'
import { WhiteSpace } from '@ant-design/react-native'
import Background from '../Image/bg.jpg'
import Headers from '../Layout/Header'
import axios from 'axios'
import {
    Containers,
    Imagebackground,
    AreaBox,
    TextHeader,
    ItemInput,
    ButonLogin
} from '../Component/StyleComponent'

class Register extends React.Component {

    state = {
        email: '',
        password: '',
        firstname: '',
        lastname: ''
    }

    SignUp = async () => {
        if (this.state.email === '' || this.state.password === '' || this.state.firstname === '' || this.state.lastname === '') {
            Alert.alert('ข้อมูลไม่ถูกต้อง , กรุณากรอกข้อมูลให้ครบ')
        }
        else {
            try {
                await axios.post('http://3.89.232.38:8888/user/addNewUser/', {
                    username:this.state.email,
                    email: this.state.email,
                    password: this.state.password,
                    firstname: this.state.firstname,
                    lastname: this.state.lastname
                })
                Alert.alert('ลงทะเบียนสำเร็จ')
                this.props.history.replace('/Login')
            } catch (error) {
                console.log('signup res', error);
            }
        }
    }


    // goToLogin = () => {
    //     this.props.history.push('./Login')
    // }

    render() {
        return (
            <Containers>
                <Headers
                    title='ลงทะเบียน'
                />
                <View style={{ flex: 1 }}>
                    <Imagebackground source={Background} style={{flex:1}}>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <AreaBox style={{ height: 400 }}>
                            <TextHeader>กรอกข้อมูล</TextHeader>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>
                            <ItemInput
                                clear
                                value={this.state.email}
                                onChange={value => this.setState({ email: value })}
                                placeholder='อีเมลล์'
                            ></ItemInput>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>
                            <ItemInput
                                clear
                                type='password'
                                value={this.state.password}
                                onChange={value => { this.setState({ password: value }) }}
                                placeholder='รหัสผ่าน'
                            ></ItemInput>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>

                            {/* <ItemInput
                            clear
                            type='password'
                            value={this.state.confirmPassword}
                            onChange={value => { this.setState({ confirmPassword: value }) }}
                            placeholder='ยืนยันรหัสผ่าน'
                        ></ItemInput>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace> */}
                            <ItemInput
                                clear
                                value={this.state.firstname}
                                onChange={value => { this.setState({ firstname: value }) }}
                                placeholder='ชื่อ'
                            ></ItemInput>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>

                            <ItemInput
                                clear
                                value={this.state.lastname}
                                onChange={value => { this.setState({ lastname: value }) }}
                                placeholder='นามสกุล'
                            ></ItemInput>

                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>

                            <ButonLogin onPress={this.SignUp}>ลงทะเบียน</ButonLogin>
                        </AreaBox>
                    </Imagebackground>
                </View>
            </Containers>
        )
    }
}

export default Register