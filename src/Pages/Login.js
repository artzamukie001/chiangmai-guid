import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, Alert } from 'react-native'
import { Icon, InputItem, WhiteSpace } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push, connectRouter } from 'connected-react-router'
import { goBack } from 'connected-react-router'
import Background from '../Image/bg.jpg'
import BackgroundLogo from '../Image/logo.jpg'
import { userLogin } from '../Reducer/Action'
import {
    Containers,
    Imagebackground,
    AreaBox,
    TextHeader,
    ItemInput,
    TextInput,
    Footers,
    ButonLogin,
    TextGeneral
} from '../Component/StyleComponent'
import axios from 'axios';

class Login extends React.Component {
    state = {
        email: '',
        password: ''
    }

    Login = async () => {
        try {
            const res = await axios.post('http://3.89.232.38:8888/auth', {
                username: this.state.email,
                password: this.state.password
            })
            const user = res.data
            await this.props.userLogin(user)
            this.setState({ isLoading: false })
            await this.props.history.push('/TabbarFooter')
        } catch (error) {
            this.setState({ isLoading: false })
            console.log(error)
            const err = error
            console.log('login response', error)
            let errorMssage = ''
            if (err.email) errorMssage += 'อีเมลล์ไม่ถูกต้อง' + err.email + '\n'
            if (err.password) errorMssage += 'รหัสผ่านไม่ถูกต้อง' + err.password + '\n'
            Alert.alert('ข้อมูลไม่ถูกต้อง', errorMssage)

        }
    }

    goToRegister = () => {
        this.props.history.push('./Register')
    }

    goToMain = () => {
        this.props.history.push('/TabbarFooter')
    }

    render() {

        const { goBack, title } = this.props

        return (
            <Containers>
                <Imagebackground source={Background} style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <AreaBox style={{ height: 505 }}>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <TextHeader>เข้าสู่ระบบ</TextHeader>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <View style={styles.logo}>
                            <Image source={BackgroundLogo} style={styles.logo} />
                        </View>

                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>


                        <ItemInput
                            placeholder='อีเมลล์'
                            value={this.state.email}
                            onChange={value => { this.setState({ email: value }) }}
                        ></ItemInput>

                        <WhiteSpace></WhiteSpace>


                        <ItemInput
                            placeholder='รหัสผ่าน'
                            value={this.state.password}
                            onChange={value => { this.setState({ password: value }) }}
                        ></ItemInput>

                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>

                        <ButonLogin onPress={this.Login}>เข้าสู่ระบบ</ButonLogin>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <WhiteSpace></WhiteSpace>
                        <TouchableOpacity onPress={goBack}>
                            <Text style={{ fontSize: 20 }}>เข้าใช้งานแบบไม่ log in</Text>
                        </TouchableOpacity>
                    </AreaBox>
                    <WhiteSpace></WhiteSpace>
                    <TouchableOpacity onPress={this.goToRegister}>
                        <TextGeneral>ลงทะเบียน</TextGeneral>
                    </TouchableOpacity>
                    <WhiteSpace></WhiteSpace>
                </Imagebackground>






            </Containers>
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        width: 150,
        height: 150,
        borderRadius: 100,

    }
})


export default connect(null, { userLogin, goBack })(Login)
