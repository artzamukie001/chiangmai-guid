import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView } from 'react-native'
import { Icon, IputItem, WhiteSpace } from '@ant-design/react-native'
import Background from '../Image/bg.jpg'
import BackgroundLogo from '../Image/logo.jpg'
import Headers from '../Layout/Header'
import Shop from '../Image/Shop.png'
import Food from '../Image/food.png'
import Travel from '../Image/travel.png'
import Hotel from '../Image/hotel.png'
import Shopping from '../Image/shopping.png'
import Bank from '../Image/bank.png'
import Law from '../Image/law.png'
import Service from '../Image/service.png'
import More from '../Image/more.png'
import Sweet from '../Image/sweet.png'

import {
    Containers,
    Imagebackground,
    AreaBox,
    TextHeader,
    ItemInput,
    TextInput,
    Footers,
    ButonLogin,
    TextGeneral
} from '../Component/StyleComponent'
import axios from 'axios';

class Login extends React.Component {
    state = {
        shopList: {}
    }

    componentDidMount() {
        this.ShopList()
    }

    ShopList = () => {
        axios
            .get('https://chiangmai.thaimarket.guide/shop')
            .then(response => {
                console.log("Response: ", response);
                this.setState({
                    shopList: response.data.data,
                    isLoading: false
                })
                console.log('State: ' + this.state.shopList)
                console.log('State.Data: ' + this.state.shopList.data)
            })
            .catch((error) => {
                console.log('ErrorNaja: ' + error);
            })
    }

    goToShopListCategoryPage = (category) => {
        console.log('this.state.shopLise.category' + category)
        return this.props.history.push('/ShopListCategoryPage', category)
    }

    goToRegister = () => {
        this.props.history.push('./Register')
    }

    render() {
        return (
            <Containers>
                <Headers
                    title='ประเภทร้านค้า'
                />

                <ScrollView>

                    <View style={{ flexDirection: 'row', backgroundColor: 'white' ,alignItems:'center',justifyContent:'center'}}>
                        <View style={{ felx: 1, alignItems: 'center', margin: 5 ,justifyContent:'center', backgroundColor:'white'}}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("ร้านอาหาร (และเครื่องดื่ม)")}>
                                <View style={styles.logo}>
                                    <Image source={Food} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>ร้านอาหาร</Text>
                        </View>

                        <View style={{ felx: 1, marginLeft: 40, alignItems: 'center'}}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("เครื่องประดับ")}>
                                <View style={styles.logo}>
                                    <Image source={Bank} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>เครื่องประดับ</Text>
                        </View>

                        <View style={{ felx: 1, marginLeft: 40, alignItems: 'center', margin: 10 }}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("คาเฟ่และของหวาน")}>
                                <View style={styles.logo}>
                                    <Image source={Sweet} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>คาเฟ่</Text>
                        </View>
                    </View>
                    
                    <WhiteSpace></WhiteSpace>
                    <View style={{ flexDirection: 'row', backgroundColor: 'white', flex: 1 }}>
                        <View style={{ felx: 1, alignItems: 'center', margin: 10 }}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("ศูนย์การค้า")}>
                                <View style={styles.logo}>
                                    <Image source={Hotel} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>ศูนย์การค้า</Text>
                        </View>

                        <View style={{ felx: 1, marginLeft: 40, alignItems: 'center', margin: 10 }}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("ของที่ระลึก")}>
                                <View style={styles.logo}>
                                    <Image source={Shopping} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>ของที่ระลึก</Text>
                        </View>

                        <View style={{ felx: 1, marginLeft: 40, alignItems: 'center', margin: 10 }}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("ต้นไม้และอุปกรณ์ทำสวน")}>
                                <View style={styles.logo}>
                                    <Image source={Travel} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>ต้นไม้และสวน</Text>
                        </View>
                    </View>
                    <WhiteSpace></WhiteSpace>
                    <View style={{ flexDirection: 'row', backgroundColor: 'white', flex: 1 }}>
                        <View style={{ felx: 1, alignItems: 'center', margin: 10 }}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("ตลาด")}>
                                <View style={styles.logo}>
                                    <Image source={Shop} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>ตลาด</Text>
                        </View>

                        <View style={{ felx: 1, marginLeft: 40, alignItems: 'center', margin: 10 }}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("บริการต่างๆ")}>
                                <View style={styles.logo}>
                                    <Image source={Service} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>บริการต่างๆ</Text>
                        </View>

                        <View style={{ felx: 1, marginLeft: 40, alignItems: 'center', margin: 10 }}>
                            <TouchableOpacity onPress={() => this.goToShopListCategoryPage("สินค้าเบ็ดเตล็ด และอื่นๆ")}>
                                <View style={styles.logo}>
                                    <Image source={More} style={styles.logo} />
                                </View>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                            <Text>เบ็ดเตล็ด</Text>
                        </View>

                        <WhiteSpace></WhiteSpace>
                    </View>

                </ScrollView>

            </Containers>
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        width: 80,
        height: 80,
        borderRadius: 100,

    }
})


export default Login
