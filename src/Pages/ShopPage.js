import React from 'react'
import StarRating from 'react-native-star-rating';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, ImageBackground } from 'react-native'
import { Icon, IputItem, WhiteSpace, TabBar, Carousel, SearchBar, Grid, Tabs, Modal, Button, Provider } from '@ant-design/react-native'
import axios from 'axios'
import Background from '../Image/bg.jpg'
import Headers from '../Layout/Header'
import MapShopPage from '../Pages/MapShopPage'
import {
    Containers,
    Imagebackground,
    Area,
    Area2,
    TextHeader,
    Logo,
    AreaBox
} from '../Component/StyleComponent'

class ShopPage extends React.Component {
    constructor(props) {
        super(props)
        this.onClose = () => {
            this.setState({
                visible: false
            })
        }

    }


    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    state = {
        shopById: {},
        product: {},
        products: {},
        starCount: 4
    }

    componentDidMount() {
        this.ShopById();
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null
                })
            },
            error => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 2000 }
        )
    }

    ShopById = () => {
        console.log('props', this.props);

        axios
            .get(`https://chiangmai.thaimarket.guide/shop/${this.props.location.state.id}`)
            .then(response => {
                console.log("Response: ", response);
                this.setState({
                    shopById: response.data.data,
                    shoplang: response.data.data,
                    isLoading: false
                })
                console.log('shopstate: ' + this.state.shopById)
            })
            .catch((error) => {
                console.log('ErrorNaja: ' + error);
            })
    }

    showProductModal = (item) => {
        this.setState({ visible: true, products: item })
        console.log('modaoProducts', item)
    }

    goToShop = () => {
        this.props.history.push('./Shop')
    }

    goToWrite = () => {
        this.props.history.push('/WriteReview')

    }

    render() {
        const tabs = [
            { title: 'รายละเอียด' },
            { title: 'โปรโมชั่น' },
            { title: 'แผนที่' },
            { title: 'รีวิว' },
        ];
        const style = {
            backgroundColor: '#fff',
            flix: 1
        };

        const shopById = this.state.shopById
        console.log('dddddddddd', this.state.shopById)
        if (this.state.shopById.lang) {
            return (
                <Containers>
                    <Provider>
                        <Headers
                            title={this.state.shopById.lang.th.name}
                        />


                        <Area>
                            <Imagebackground source={{ uri: shopById.image }}>
                                <View style={{
                                    backgroundColor: 'rgba(0, 0, 0, 0.7)', width: 360,
                                    height: 160
                                }}>
                                    <Text style={{ margin: 5, fontSize: 30, color: 'white' }}>{this.state.shopById.lang.th.name}</Text>
                                    {/* <View style={{backgroundColor:'gray',width:200,height:50}}>
                                        <StarRating
                                            disabled={false}
                                            maxStars={5}
                                            rating={this.state.starCount}
                                            selectedStar={(rating) => this.onStarRatingPress(rating)}
                                        />
                                    </View> */}
                                </View>
                            </Imagebackground>
                        </Area>

                        <View style={{ flex: 1 }}>
                            <Tabs tabs={tabs}>
                                <View style={style}>
                                    <ScrollView>
                                        <Text style={{ margin: 5, fontSize: 20 }}>{this.state.shopById.lang.th.name}</Text>
                                        <Text style={{ marginLeft: 5 }}>{this.state.shopById.lang.th.description}</Text>
                                        <WhiteSpace></WhiteSpace>
                                        <Text style={{ margin: 5, fontSize: 20 }}>สินค้า</Text>
                                        <WhiteSpace></WhiteSpace>
                                        <View>
                                            <Grid
                                                horizontal={true}
                                                isCarousel
                                                carouselMaxRow={1}
                                                columnNum={2}
                                                data={this.state.shopById.products}
                                                itemStyle={{ height: 300, width: 100 }}
                                                renderItem={(item) => (
                                                    <TouchableOpacity onPress={() => this.showProductModal(item)}>
                                                        <Image style={{ width: 170, height: 200, marginLeft: 5 }} source={{ uri: item.image }} />
                                                        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                                            <Text>{item.lang.th.name}</Text>

                                                        </View>
                                                    </TouchableOpacity>
                                                )}
                                            />
                                        </View>

                                        <Modal
                                            transparent={true}
                                            visible={this.state.visible}
                                            animationType="slide-up"
                                            onClose={this.onClose}
                                        >
                                            <View>
                                                <Icon name={'close-circle'} size={40} color={'#C0C0C0'} style={{ marginLeft: 218, marginTop: -20 }} onPress={this.onClose} />
                                                <WhiteSpace></WhiteSpace>
                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                    <Image style={{ width: 170, height: 210 }} source={{ uri: this.state.products.image }} />
                                                    <Text style={{ marginTop: 15, marginBottom: 15 }}>{this.state.products.lang ? this.state.products.lang.th.name : ''}</Text>
                                                    <Text style={{ marginLeft: 20, marginBottom: 15 }}>"{this.state.products.lang ? this.state.products.lang.th.description : ''}"</Text>
                                                </View>
                                            </View>
                                        </Modal>

                                    </ScrollView>
                                </View>

                                <View style={style}>
                                    <Text style={{ fontSize: 30 }}>Comming Soon ...</Text>
                                </View>


                                <View style={{ flex: 1 }}>
                                    <MapView
                                        showsUserLocation={true}
                                        showsMyLocationButton={true}
                                        showsCompass={true}
                                        toolbarEnabled={true}
                                        zoomEnabled={true}
                                        rotateEnabled={true}
                                        style={{ flex: 1 }}
                                        initialregion={{
                                            latitude: 18.80420416781437,
                                            longitude: 98.9490090635145,
                                            latitudeDelta: 0.01,
                                            longitudeDelta: 0.0121,
                                        }}
                                    >
                                        <Marker coordinate={{
                                            latitude: Number(this.state.shopById.location.latitude),
                                            longitude: Number(this.state.shopById.location.longitude)
                                        }} />
                                    </MapView>
                                </View>


                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <ScrollView>
                                        <WhiteSpace></WhiteSpace>
                                        <Button
                                            style={{ borderRadius: 100, width: 100, justifyContent: 'center', marginLeft: 130 }}
                                            onPress={this.goToWrite}>เขียนรีวิว</Button>
                                        <WhiteSpace></WhiteSpace>
                                        {/* <Grid
                                            columnNum={1}
                                            itemStyle={{ height: 260,backgroundColor:'gray' }}
                                        /> */}
                                        <View style={{ height: 120, width: 350, backgroundColor: 'gray' }}>
                                            <WhiteSpace></WhiteSpace>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name={'user'} size={40} color={'black'} />
                                                <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                                                    <Text>ชื่อ-นามสกุผู้ใช้</Text>
                                                    <Icon name={'star'} />
                                                    <View style={{ justifyContent: 'center', alignItems: 'center', margin: 5 }}>
                                                        <Text style={{ fontSize: 20 }}>คอมเม้นท์ต่างๆก็ว่ากันไป</Text>
                                                    </View>
                                                </View>

                                            </View>
                                        </View>
                                        <WhiteSpace></WhiteSpace>
                                        <View style={{ height: 120, width: 350, backgroundColor: 'gray' }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name={'user'} size={40} color={'black'} />
                                                <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                                                    <Text>ชื่อ-นามสกุผู้ใช้</Text>
                                                    <Icon name={'star'} />
                                                    <View style={{ justifyContent: 'center', alignItems: 'center', margin: 5 }}>
                                                        <Text style={{ fontSize: 20 }}>คอมเม้นท์ต่างๆก็ว่ากันไป</Text>
                                                    </View>
                                                </View>

                                            </View>
                                        </View>
                                        <WhiteSpace></WhiteSpace>
                                        <View style={{ height: 120, width: 350, backgroundColor: 'gray' }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name={'user'} size={40} color={'black'} />
                                                <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                                                    <Text>ชื่อ-นามสกุผู้ใช้</Text>
                                                    <Icon name={'star'} />
                                                    <View style={{ justifyContent: 'center', alignItems: 'center', margin: 5 }}>
                                                        <Text style={{ fontSize: 20 }}>คอมเม้นท์ต่างๆก็ว่ากันไป</Text>
                                                    </View>
                                                </View>

                                            </View>
                                        </View>
                                        <WhiteSpace></WhiteSpace>
                                    </ScrollView>
                                </View>

                            </Tabs>
                        </View>
                    </Provider>

                </Containers >
            )
        } else {
            return (

                <View>
                    <Text>asdfdfd</Text>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        // height: '100%',
        // width: 400,
        // justifyContent: 'center',
        // alignItems: 'center',
        flex: 1
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});
export default ShopPage
