import React from 'react'
import StarRating from 'react-native-star-rating'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, ImageBackground, Alert } from 'react-native'
import { Icon, TextareaItem, WhiteSpace, TabBar, Carousel, SearchBar, Grid, Tabs, Modal, Button, Provider } from '@ant-design/react-native'
import axios from 'axios'
import Headers from '../Layout/Header'
import { connect } from 'react-redux'
import has from 'lodash/has'
import {
    Containers,
    Imagebackground,
    Area,
    Area2,
    TextHeader,
    Logo
} from '../Component/StyleComponent'

class WriteReview extends React.Component {

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    state = {
        shopById: {},
        product: {},
        products: {},
        starCount: 2.5,
        productsDescript: [],
        review: [],
        newReview: [],
        date: '',
        reviewScore: 0.0,
        reviewComment: '',
        visible: false,
        visibleEdit: false,
        reviewIdReviewComment: '',
        reviewIdReviewScore: 0.0,
        reviewId: '',
        reviewDelete: true,
    }

    // componentDidMount() {
    //     this.getReviewByShop();
    // }

    // getReviewByShop = () => {
    //     axios({
    //         url: `http://3.89.232.38:8888/Shop/shopId/${this.props.history.location.state.id}`,
    //         method: 'get',
    //     }).then(response => {
    //         console.log('getReviewByShop : ', response);
    //         this.setState({
    //             review: response.data,
    //         })
    //     }).catch((error) => {
    //         console.log('ErrorGetReviewNaja: ' + error);
    //     })
    // }

    addReview = () => {
        console.log('Bearer', this.props);
        console.log('Reviews!!', this.props.user.user.id)
        
        
        
        if (!has(this.props, 'user.user.id')) {
            Alert.alert('ยังไม่ได้เข้าสู่ระบบ', 'กรุณาเข้าสู่ระบบ')
        } else {
            axios({
                url: `http://3.89.232.38:8888/addReview/${this.props.user.user.id}`,
                method: 'post',
                headers: { 'Authorization': `Bearer ${this.props.user.token}` },
                data: {
                    date: Date.now(),
                    // shopId: this.props.history.location.state.id,
                    reviewScore: this.state.reviewScore,
                    reviewComment: this.state.reviewComment,
                }
                
            }).then(response => {
                console.log("stateeeeee",this.props.history.location.state);
                this.setState({
                    visible: false,
                    visibleEdit: false,
                    review: [response.data, ...this.state.review]
                })
                console.log('NewReviews', ...this.state.newReview)
                console.log('Reviews', ...this.state.review)
                console.log('response', response.data)
            }).catch(error => {
                console.log(error);
            })
        }
    }

    render() {
        return (
            <Containers>
                <Headers
                    title='เขียนรีวิว'
                />
                <View style={{ height: 120, width: 360, backgroundColor: 'gray', justifyContent: 'center', alignItems: 'center' }}>
                    <StarRating
                        starStyle={marginLeft = 50}
                        starSize={40}
                        disabled={false}
                        maxStars={5}
                        emptyStarColor='gray'
                        fullStarColor='red'
                        rating={this.state.starCount}
                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                    />
                    <WhiteSpace></WhiteSpace>
                    <Text style={{ fontSize: 20 }}>ให้คะแนน</Text>
                </View>
                <View style={{ margin: 5 }}>
                    <TextareaItem
                        rows={3}
                        placeholder='รายละเอียด'
                    />
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Button style={{ borderRadius: 100, width: 100, marginTop: 5 }} onPress={this.addReview}>โพส</Button>
                </View>
            </Containers>
        )
    }




}
const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
}

export default connect(mapStateToProps, null)(WriteReview)
