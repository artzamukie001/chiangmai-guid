import React, { Component } from 'react'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, ImageBackground } from 'react-native'
import axios from 'axios'
import { connect } from 'react-redux'
import { push, connectRouter } from 'connected-react-router'
import { goBack } from 'connected-react-router'
import {
    Containers, Area
} from '../Component/StyleComponent'


class MapPage extends React.Component {
    getImageIcon = (item) => {
        if (item === ("ร้านอาหาร (และเครื่องดื่ม)")) {
            return require('../Image/iconmap/food.png')
        } else if (item === ("เครื่องประดับ")) {
            return require('../Image/iconmap/bank.png')
        } else if (item === ("คาเฟ่และของหวาน")) {
            return require('../Image/iconmap/sweet.png')
        } else if (item === ("ศูนย์การค้า")) {
            return require('../Image/iconmap/hotel.png')
        } else if (item === ("ของที่ระลึก")) {
            return require('../Image/iconmap/shopping.png')
        } else if (item === ("ต้นไม้และอุปกรณ์ทำสวน")) {
            return require('../Image/iconmap/travel.png')
        }
        else if (item === ("ตลาด")) {
            return require('../Image/iconmap/Shop.png')
        }
        else if (item === ("บริการต่างๆ")) {
            return require('../Image/iconmap/service.png')
        }
        else if (item === ("สินค้าเบ็ดเตล็ด และอื่นๆ")) {
            return require('../Image/iconmap/more.png')
        } else if (item === ("อาหารไทย")) {
            return require('../Image/iconmap/food.png')
        } else if (item === ("อาหารเอเชีย")) {
            return require('../Image/iconmap/food.png')
        } else if (item === ("ร้านค้า")) {
            return require('../Image/iconmap/Shop.png')
        }
        else if (item === ("สุขภาพและความงาม")) {
            return require('../Image/iconmap/hospital.png')
        } else if (item === ("เฟอร์นิเจอร์และของตกแต่งบ้าน")) {
            return require('../Image/iconmap/tools.png')
        }
        else if (item === ("แฟชั่นบุรุษ")) {
            return require('../Image/iconmap/boy.png')
        }
        else if (item === ("แฟชั่นสตรี")) {
            return require('../Image/iconmap/girl.png')
        }
        else if (item === ("ของเก่า ของสะสม")) {
            return require('../Image/iconmap/television.png')
        }
    }

    state = {
        shopList: {},
        item: {},
        current: {
            latitude: 0,
            longitude: 0,
            error: null
        }
    }

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    // clickShop = (item) => {
    //     const { clickShop } = this.props
    //     console.log('item', item);
    //     clickShop(item)

    // }
    onClickShop = (id) => {
        console.log('alsfjlksjdflkajsdfkljsdf', this.props);
        const { push } = this.props
        push('/ShopPage', { id })
    }

    componentDidMount() {
        this.ShopList()
    }

    currentLocation = () => {
        navigator.geolocation.getCurrentPosition(position => {
            this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                error: null
            })
        }
        );

    }

    ShopList = () => {
        axios
            .get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0')
            .then(response => {
                console.log("Response: ", response);
                this.setState({
                    shopList: response.data.data,
                    isLoading: false,
                    item: response.data.data[0]
                })
                console.log('State: ' + this.state.shopList)
                console.log('State.Data: ' + this.state.shopList.data)
            })
            .catch((error) => {
                console.log('ErrorNaja: ' + error);
            })
            .finally(() => { console.log('Finally') })
    }

    getLocation() {
        const maps = this.state.shopList
        let location = []
        if (maps.length !== undefined) {
            console.log('length', maps.length);
            for (let i = 0; i < maps.length; i++) {
                console.log('latitude', Number(maps[i].location.latitude));
                console.log('longitude', Number(maps[i].location.longitude));
                location.push(
                    <Marker
                        onPress={() => this.onClickShop(maps[i].id)}
                        title={maps[i].lang.th.name}
                        image={this.getImageIcon(maps[i].category)}
                        coordinate={{
                            latitude: Number(maps[i].location.latitude),
                            longitude: Number(maps[i].location.longitude)
                        }}
                    />
                )
            }
            return location
        }
    }

    render() {
        console.log('whatthefuck', this.props);

        return (
            <Containers>

                <View style={styles.container}>
                    <MapView
                        onPress={this.onClose}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        showsCompass={true}
                        toolbarEnabled={true}
                        zoomEnabled={true}
                        rotateEnabled={true}
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={styles.map}
                        region={{
                            latitude: 18.80420416781437,
                            longitude: 98.9490090635145,
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.0121,
                        }}
                    >

                        {this.getLocation()}

                    </MapView>
                    <Marker coordinate={this.state.current} />
                </View>


            </Containers>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        // height: '100%',
        // width: 400,
        // justifyContent: 'center',
        // alignItems: 'center',
        // flex: 1
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});

export default connect(null, { push })(MapPage)