import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, ImageBackground, Linking } from 'react-native'
import { Icon, IputItem, WhiteSpace, TabBar, Carousel, SearchBar } from '@ant-design/react-native'
import Background from '../Image/cm6.jpg'
import ImgC1 from '../Image/cm1.jpg'
import Promote from '../Image/cm4.jpg'
import ImgC2 from '../Image/cm7.jpg'
import ImgC3 from '../Image/cm9.jpg'
import Shop from '../Image/Shop.png'
import More from '../Image/more.png'
import BgIcon from '../Image/bg.jpg'
import { connect } from 'react-redux'
import { push, connectRouter } from 'connected-react-router'

import {
    Containers,
    Imagebackground,
    Area,
    Area2,
    TextHeader,
    Logo
} from '../Component/StyleComponent'
import Footers from '../Layout/TabbarFooter'

class Main extends React.Component {

    goToShop = () => {
        this.props.push('./Shop')
    }

    goToCatagory = () => {
        this.props.push('./Catagory')
    }

    render() {
        return (
            <Containers>

                <Area>
                    <SearchBar
                        // value={this.state.value}
                        placeholder="ค้นหา"
                    // onSubmit={value => Alert.alert(value)}
                    // onCancel={this.clear}
                    // onChange={this.onChange}
                    // showCancelButton
                    />
                </Area>
                <ScrollView>
                    <Imagebackground source={Background}>

                        <TextHeader style={{ color: 'white' }}></TextHeader>
                        <TextHeader style={{ color: 'white' }}></TextHeader>
                        <WhiteSpace></WhiteSpace>
                    </Imagebackground>

                    {/* <Containers style={{ marginTop: 5 }}>
                        <Imagebackground source={BgIcon}> */}
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', flex: 0, marginTop: 5,backgroundColor:'#F8F8FF' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                                    <TouchableOpacity onPress={this.goToShop}>
                                        <View style={styles.logo}>
                                            <Image source={Shop} style={styles.logo} />
                                        </View>
                                    </TouchableOpacity>
                                    <WhiteSpace></WhiteSpace>
                                    <Text>ร้านค้า</Text>
                                </View>

                                <View style={{ marginLeft: 50, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                                    <TouchableOpacity onPress={this.goToCatagory}>
                                        <View style={styles.logo}>
                                            <Image source={More} style={styles.logo} />
                                        </View>
                                    </TouchableOpacity>
                                    <WhiteSpace></WhiteSpace>
                                    <Text>อื่นๆ</Text>
                                </View>
                            </View>
                        {/* </Imagebackground>
                    </Containers> */}


                    <Area style={{ padding: 5 }}>
                        <Carousel
                            style={styles.wrapper}
                            selectedIndex={2}
                            autoplay
                            infinite
                            afterChange={this.onHorizontalSelectedIndexChange}

                        >
                            <View>
                                <TouchableOpacity onPress={() => Linking.openURL('http://travel.trueid.net/detail/K2qVnyVo7MO')}>
                                    <Image style={{ width: '100%', height: '100%' }} source={Promote} />
                                </TouchableOpacity>
                            </View>

                        </Carousel>
                    </Area>

                    <Area style={{ padding: 5 }}>
                        <Carousel
                            style={styles.wrapper}
                            selectedIndex={2}
                            autoplay
                            infinite
                            afterChange={this.onHorizontalSelectedIndexChange}

                        >
                            <View>
                                <Image style={{ width: '100%', height: '100%' }} source={ImgC1} />
                            </View>

                            <View>
                                <Image style={{ width: '100%', height: '100%' }} source={ImgC2} />
                            </View>
                            <View>
                                <Image style={{ width: '100%', height: '100%' }} source={ImgC3} />
                            </View>
                        </Carousel>
                    </Area>
                </ScrollView>
            </Containers>
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        width: 100,
        height: 100,
        borderRadius: 100,

    },
    wrapper: {
        backgroundColor: '#fff',
        height: 150,
    },
    containerHorizontal: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
    },
    containerVertical: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
    },
    text: {
        color: '#fff',
        fontSize: 36,
    },
    logo: {
        width: 100,
        height: 100,
        borderRadius: 100,
        marginLeft: 0,
        backgroundColor: 'gray'

    }
})

export default connect(null, { push })(Main)
