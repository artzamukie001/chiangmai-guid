import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, ImageBackground } from 'react-native'
import { Icon, IputItem, WhiteSpace, TabBar, Carousel, SearchBar, Button } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import Background from '../Image/bg.jpg'
import BackgroundLogo from '../Image/logo.jpg'
import {
    Containers,
    Imagebackground,
    Area,
    ItemInput,
    TextHeader,
    Logo,
    Buttons,
    ButtonFacebook,
    TextInput
} from '../Component/StyleComponent'
import Footers from '../Layout/TabbarFooter'
import { userLogout } from '../Reducer/Action'

class Profile extends React.Component {

    ProfileLogin = () => {
        console.log('this.props.user.user', this.props.user);
    }

    goToRegister = () => {
        this.props.push('./Register')
    }

    goToLogin = () => {
        this.props.push('./Login')
    }

    goToMain = () => {
        this.props.userLogout()
       return this.props.push('./TabbarFooter')
    }


    render() {
        console.log("userrrrrrrrrrrr", this.props.user);

        if (!this.props.user.user) {
            return (
                <Containers>
                    <Area>
                        <Imagebackground source={Background}>
                            <TextHeader>ยังไม่ได้เข้าสู่ระบบ</TextHeader>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>
                            <Buttons onPress={this.goToLogin}>เข้าสู่ระบบ</Buttons>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>
                            <ButtonFacebook>เข้าสู่ระบบด้วย Facebook</ButtonFacebook>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>
                            <TouchableOpacity onPress={this.goToRegister}>
                                <TextInput>ลงทะเบียน</TextInput>
                            </TouchableOpacity>
                            <WhiteSpace></WhiteSpace>
                        </Imagebackground>
                    </Area>
                </Containers>
            )
        } else {
            return (
                <Containers>
                    <Area>
                        <Imagebackground source={Background}>
                            <TextHeader>ข้อมูลส่วนตัว</TextHeader>
                            <WhiteSpace></WhiteSpace>
                            <View style={styles.logo}>
                                <Image source={BackgroundLogo} style={styles.logo} />
                            </View>
                            <WhiteSpace></WhiteSpace>
                        </Imagebackground>
                    </Area>
                    <WhiteSpace></WhiteSpace>
                    <Area style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>ชื่อ:  {this.props.user.user.firstname}</Text>
                        <WhiteSpace></WhiteSpace>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>นามสกุล:  {this.props.user.user.lastname}</Text>
                        <WhiteSpace></WhiteSpace>
                        <Button onPress={this.goToMain}>ออกจากระบบ</Button>
                    </Area>
                </Containers>
            )
        }
    }
}

const styles = StyleSheet.create({
    logo: {
        width: 200,
        height: 200,
        borderRadius: 100,

    },
    wrapper: {
        backgroundColor: '#fff',
        height: 150
    },
    containerHorizontal: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
    },
    containerVertical: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
    },
    text: {
        color: '#fff',
        fontSize: 36,
    },
    logo: {
        width: 200,
        height: 200,
        borderRadius: 100,
        marginLeft: 0,
        backgroundColor: 'gray'

    }
})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

// const mapDispatchToProps = (dispatch) => {
//     return {
//         userLogout: () => {
//             dispatch({
//                 type: 'USER_LOGOUT'
//             })
//         }
//     }
// }

export default connect(mapStateToProps, { userLogout, push })(Profile)



