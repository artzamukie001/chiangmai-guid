import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
// import enUS from '@ant-design/react-native/lib/locale-provider/en_US';
import { store, history } from './AppStore'
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import Login from './src/Pages/Login'
import Register from './src/Pages/Register'
import Profile from './src/Pages/Profile'
import Main from './src/Pages/Main'
import MapPage from './src/Pages/MapPage'
import Shop from './src/Pages/Shop'
import Catagory from './src/Pages/Catagory'
import ShopPage from './src/Pages/ShopPage'
import TabbarFooter from './src/Layout/TabbarFooter'
import WriteReview from './src/Pages/WriteReview'
import ShopListCategoryPage from './src/Pages/ShopListCategoryPage'


console.log(store)

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/Login" component={Login} />
                        <Route exact path="/Register" component={Register} />
                        <Route exact path="/Profile" component={Profile} />
                        <Route exact path="/Main" component={Main} />
                        <Route exact path="/MapPage" component={MapPage} />
                        <Route exact path="/Shop" component={Shop} />
                        <Route exact path="/Catagory" component={Catagory} />
                        <Route exact path="/ShopPage" component={ShopPage} />
                        <Route exact path="/TabbarFooter" component={TabbarFooter} />
                        <Route exact path="/WriteReview" component={WriteReview} />
                        <Route exact path="/ShopListCategoryPage" component={ShopListCategoryPage} />
                        <Redirect to="/TabbarFooter" />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}

export default Router
